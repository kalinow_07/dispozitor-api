<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/registration', [\App\Http\Controllers\User\UserController::class, 'registration']);
Route::post('/login', [\App\Http\Controllers\User\UserController::class, 'login']);
Route::post('/chain/get', [\App\Http\Controllers\Chain\ChainController::class, 'get']);
Route::get('/test', function (){
    return new \Illuminate\Http\Response("123");
});
Route::prefix('personal_interpretation')->middleware("auth:api")->group(function (){
    Route::post('/create', [\App\Http\Controllers\Interpretation\PersonalInterpretationController::class, 'create']);
    Route::get('/', [\App\Http\Controllers\Interpretation\PersonalInterpretationController::class, 'search']);
    Route::get('/{id}', [\App\Http\Controllers\Interpretation\PersonalInterpretationController::class, 'get']);
    Route::put('/{id}', [\App\Http\Controllers\Interpretation\PersonalInterpretationController::class, 'edit']);
    Route::delete('/{id}', [\App\Http\Controllers\Interpretation\PersonalInterpretationController::class, 'remove']);
});
Route::prefix('interpretation')->group(function (){
    Route::get('/', [\App\Http\Controllers\Interpretation\InterpretationController::class, 'search']);
});
Route::prefix('personal_card')->middleware("auth:api")->group(function (){
    Route::get('', [\App\Http\Controllers\PersonalCard\PersonalCardController::class, 'get']);
    Route::put('/{id}', [\App\Http\Controllers\PersonalCard\PersonalCardController::class, 'edit']);
});
//Route::middleware('auth:api')->post('/registration', function (Request $request) {
//    return response()->json(['response' => true]);
//});
Route::apiResource('questions', \App\Http\Controllers\Question\QuestionController::class);
