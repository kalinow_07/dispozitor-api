<?php

namespace App\Http\Controllers\Interpretation;

use App\Http\Controllers\Controller;
use App\Models\Interpretation;
use App\Models\InterpretationLink;
use App\Service\Interpretation\IndexInterpretation\IndexInterpretationService;
use App\Service\Interpretation\RatingInterpretation\RatingInterpretationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PersonalInterpretationController extends Controller
{
    public function create(Request $request)
    {
        $interpretation = Interpretation::firstOrCreate([
            "name" => $request->get("name"),
            "user_id" => Auth::id(),
            "text" => $request->get("text"),
            "link" => $request->get("link"),
            "clip" => $request->get("clip"),
            "options" => collect($request->get("planetSelectOption"))->toJson(),
            "index" => "",
            "rating" => 0,
        ]);
        $indexInterpretationService = new IndexInterpretationService();
        $indexInterpretationService->create($interpretation);
        $ratingInterpretationService = new RatingInterpretationService();
        $ratingInterpretationService->setRating($interpretation);
        if((bool)$request->get("interpretation_link")){
            InterpretationLink::create([
                "link" => $request->get("interpretation_link"),
                "interpretation_id" => $interpretation->id,
            ]);
        }
        if((bool)$request->get("interpretation_clip")){
            InterpretationLink::create([
                "link" => $request->get("interpretation_clip"),
                "interpretation_id" => $interpretation->id,
            ]);
        }
        return response()->json($interpretation);
    }

    public function search(Request $request)
    {
        $param = $request->only(['planetId','signId','orbitId','chainTypeId']);
        $query = Interpretation::where("user_id", Auth::id());
        if(isset($param["planetId"])){
            $planetId = $param["planetId"];
            $query->whereRaw("JSON_CONTAINS(options,
            JSON_OBJECT('planets', JSON_ARRAY(JSON_OBJECT('active',true,'planet', JSON_OBJECT('id','$planetId')))))");
        }
        if(isset($param["signId"])){
            $signId = $param["signId"];
            $query->whereRaw("JSON_CONTAINS(options,
            JSON_OBJECT('planets', JSON_ARRAY(JSON_OBJECT('active',true,'sign', JSON_OBJECT('id','$signId')))))");
        }
        if(isset($param["orbitId"])){
            $orbitId = $param["orbitId"];
            $query->whereRaw("JSON_CONTAINS(options,
            JSON_OBJECT('planets', JSON_ARRAY(JSON_OBJECT('active',true,'orbit', JSON_OBJECT('id','$orbitId')))))");
        }
        if(isset($param["chainTypeId"])){
            $chainTypeId = $param["chainTypeId"];
            $query->whereRaw("JSON_CONTAINS(options,
            JSON_OBJECT('chainType', JSON_OBJECT('id','$chainTypeId')))");
        }
        $query->limit(120);
        $query->orderBy("id", "DESC");
        $result = $query->get();
        return response()->json($result);
    }

    public function get($id, Request $request)
    {
        $interpretation = Interpretation::findOrFail($id);
        return $interpretation;
    }

    public function remove($id, Request $request)
    {
        return Interpretation::destroy($id);
    }

    public function edit($id, Request $request)
    {
        $param = $request->only(["name", "text", "link", "clip", "planetSelectOption"]);
        Interpretation::where("id", $id)->update([
            "name" => $param["name"],
            "text" => isset($param["text"]) ? $param["text"] : null,
            "link" => isset($param["link"]) ? $param["link"] : null,
            "clip" => isset($param["clip"]) ? $param["clip"] : null,
            "options" => $param["planetSelectOption"],
        ]);
        $indexInterpretationService = new IndexInterpretationService();
        $indexInterpretationService->create(Interpretation::find($id));
        $ratingInterpretationService = new RatingInterpretationService();
        $ratingInterpretationService->setRating(Interpretation::find($id));
        return Interpretation::findOrFail($id);
    }
}
