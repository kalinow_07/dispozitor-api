<?php

namespace App\Http\Controllers\Interpretation;

use App\Http\Controllers\Controller;
use App\Models\Interpretation;
use App\Service\Interpretation\IndexInterpretation\IndexInterpretationService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InterpretationController extends Controller
{
    public function search(Request $request){
        $param = $request->only(["date", "time", "chainCondition"]);
        $chainCondition = json_decode($param["chainCondition"], true);
        $query = DB::table("interpretations")->select("id");
        $indexInterpretationService = new IndexInterpretationService();
        $inverse_indexes = $indexInterpretationService->getInverseIndexByChain(Carbon::parse($param["date"])->addDay(), Carbon::parse($param["time"]), $chainCondition);
        $query->where('index', 'not regexp', implode("|", $inverse_indexes));
        if(isset($chainCondition["planets"]) && count($chainCondition["planets"]) > 0){
            foreach ($chainCondition["planets"] as $planet_index){
                $query->where('index', 'like', '%' . $planet_index . '%');
            }
        }
        $query->orderBy("rating", "DESC");
        $result = $query->get();
        foreach ($result as $key => $item){
            $interpretation = Interpretation::find($item->id);
            $interpretation->links;
            $interpretation->clips;
            $interpretation->user;
            $result[$key] = $interpretation;
        }
        return response()->json($result);
    }
}
