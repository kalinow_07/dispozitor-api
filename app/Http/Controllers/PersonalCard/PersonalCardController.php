<?php

namespace App\Http\Controllers\PersonalCard;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PersonalCardController extends Controller
{
    public function get(Request $request){
        return User::findOrFail(Auth::id());
    }
    public function edit($id, Request $request){
        $param = $request->only(["name", "page", "avatar", "profession"]);
        User::where("id", $id)->update($param);
        return User::findOrFail($id);
    }
}
