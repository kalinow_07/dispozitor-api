<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function registration(Request $request){
        $param = $request->only(['name', 'email', 'password']);
        return User::forceCreate([
            'name' => $param['name'],
            'email' => $param['email'],
            'password' => Hash::make($param['password']),
            'api_token' => Str::random(80),
        ]);
    }
    public function login(Request $request){
        try{
            $param = $request->only(["email", "password"]);
            if (Auth::attempt(['email' => $param["email"], 'password' => $param["password"]])) {
                return Auth::user();
            }else{
                return response()->json(["error" => "Неправильный логини или пароль"]);
            }
        }catch(\Exception $exception){
            return response()->json(["error" => $exception->getMessage()]);
        }
    }
}
