<?php

namespace App\Http\Controllers\Chain;

use App\Http\Controllers\Controller;
use App\Service\Chain\ChainService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ChainController extends Controller
{
    public function get(Request $request){
        $param = $request->only(["chainDate", "chainType"]);
        $date = Carbon::parse($param["chainDate"]["date"])->addDay();
        $time = Carbon::parse($param["chainDate"]["time"]);
        $chainService = new ChainService();
        $result = $chainService->get($date, $time, $param["chainType"]);
        return response()->json(["response" => $result]);
    }
}
