<?php


namespace App\Service\Interpretation\RatingInterpretation;


use App\Models\Interpretation;
use App\Models\Orbit;

class RatingInterpretationService
{

    public function setRating(Interpretation $interpretation){

        // Рейтинг толкования
        // Чем ближе к центру, тем больше рейтинг
        $result = 0;
        $min_orbit = 10;
        foreach (Orbit::all() as $orbit){
            if(strstr($interpretation->index, $orbit->index) !== false){
                if((int)$orbit->index < $min_orbit){
                    $min_orbit = $orbit->index;
                }
            }
        }
        $result = 100 / ((integer)$min_orbit + 1);
        // Чем конкретнее указано, тем больше рейтинг
        $result += strlen($interpretation->index);
        $interpretation->rating = $result;
        $interpretation->save();

    }

}
