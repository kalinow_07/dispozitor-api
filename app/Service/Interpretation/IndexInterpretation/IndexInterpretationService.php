<?php


namespace App\Service\Interpretation\IndexInterpretation;


use App\Models\Interpretation;
use App\Models\Orbit;
use App\Models\Planet;
use App\Models\Sign;
use App\Service\Chain\ChainService;
use App\Service\Interpretation\SearchInterpretation\SearchInterpretationChainAdapterService;
use App\Service\Interpretation\SearchInterpretation\SearchInterpretationInterpretationAdapterService;
use Carbon\Carbon;

class IndexInterpretationService
{

    private $interpretationOption;
    private $chainData;
    private $planets;
    private $signs;
    private $orbits;

    public function __construct(){
        $this->planets = Planet::all();
        $this->signs = Sign::all();
        $this->orbits = Orbit::all();
    }


    public function create(Interpretation $interpretation){
        $searchInterpretationInterpretationAdapterService = new SearchInterpretationInterpretationAdapterService();
        $this->interpretationOption = $searchInterpretationInterpretationAdapterService->get(json_decode($interpretation->options, true)["planets"]);
        $result = "-";
        foreach ($this->interpretationOption as $item){
            $planet = $item["planet"];
            $sign = $item["sign"];
            $orbit = $item["orbit"];
            $retro = $item["retro"];
            if($sign !== false){
                $result .= $planet . "_" . $sign . "-";
            }
            if($orbit !== false){
                $result .= $planet . "_" . $orbit . "-";
            }
            if($retro === false){
                $result .= $planet . "_der-";
            }
            if($retro === true){
                $result .= $planet . "_ret-";
            }
        }
        $interpretation->index = $result;
        $interpretation->save();
    }
    public function getInverseIndexByChain(Carbon $date, Carbon $time, $chainCondition){
        $chainService = new ChainService();
        $result = $chainService->get($date, $time, $chainCondition["chain_type"]);
        $searchInterpretationChainAdapterService = new SearchInterpretationChainAdapterService();
        $this->chainData = $searchInterpretationChainAdapterService->get(json_decode($result, true)["result"]["nodes"]);

        $result = [];
        foreach ($this->chainData as $item){
            $result = array_merge($result, $this->getInverseBySign($item["planet"], $item["sign"]));
            $result = array_merge($result, $this->getInverseByOrbit($item["planet"], $item["orbit"]));
            $result = array_merge($result, $this->getInverseByRetro($item["planet"], $item["retro"]));
        }
        return $result;
    }
    private function getInverseBySign(string $planet_index, string $sign_index){
        $result = [];
        foreach ($this->signs as $sign){
            if($sign->index !== $sign_index){
                $result[] = "-" . $planet_index . "_" . $sign->index . "-";
            }
        }
        return $result;
    }
    private function getInverseByOrbit(string $planet_index, string $orbit_index){
        $result = [];
        foreach ($this->orbits as $orbit){
            if((string)$orbit->index !== (string)$orbit_index){
                $result[] = "-" . $planet_index . "_" . $orbit->index . "-";
            }
        }
        return $result;
    }
    private function getInverseByRetro(string $planet_index, $retro){
        $result = [];
        if($retro === true){
            $result[] = "-" . $planet_index . "_der-";
        }
        if($retro === false){
            $result[] = "-" . $planet_index . "_ret-";
        }
        return $result;
    }
}
