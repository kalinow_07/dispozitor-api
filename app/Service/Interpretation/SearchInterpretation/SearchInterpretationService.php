<?php


namespace App\Service\Interpretation\SearchInterpretation;


use App\Models\Interpretation;
use App\Service\Chain\ChainService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SearchInterpretationService
{

    private $max_count = 10; // Максимальное количество толкований
    private $result = [];

    private $searchInterpretationChainAdapterService;
    private $searchInterpretationInterpretationAdapterService;

    private $chainData;
    private $interpretationOption;
    public function __construct()
    {
        $this->searchInterpretationChainAdapterService = new SearchInterpretationChainAdapterService();
        $this->searchInterpretationInterpretationAdapterService = new SearchInterpretationInterpretationAdapterService();
    }

    // Совпадает ли цепочка с одним толкованием
    private function isCoincidence($chainData, Interpretation $interpretation){
        $this->chainData = $this->searchInterpretationChainAdapterService->get(json_decode($chainData, true)["result"]["nodes"]);
        $this->interpretationOption = $this->searchInterpretationInterpretationAdapterService->get(json_decode($interpretation->options, true)["planets"]);
        dump($this->chainData);
        dump($this->interpretationOption);
        foreach ($this->interpretationOption as $item){
            if(!array_search($item, $this->chainData)){
                return false;
            }
        }
        return true;
    }

    public function search(Carbon $date, Carbon $time){

        $chainService = new ChainService();
        $chainData = $chainService->get(Carbon::now(), Carbon::now());
        $this->chainData = $this->searchInterpretationChainAdapterService->get(json_decode($chainData, true)["result"]["nodes"]);
        //$this->interpretationOption = $this->searchInterpretationInterpretationAdapterService->get(json_decode($interpretation->options, true)["planets"]);




    }

}
