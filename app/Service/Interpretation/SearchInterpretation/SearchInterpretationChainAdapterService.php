<?php


namespace App\Service\Interpretation\SearchInterpretation;


class SearchInterpretationChainAdapterService
{
    /**
     * @param $data
     * @return mixed
     * Need return [["sign" => "tel","planet" => "sol","retro" => true, "orbit" => 2]]
     */
    public function get(Array $data){
        $result = [];
        foreach ($data as $item){
            $result[] = [
                "planet" => $item["marker"],
                "sign" => substr($item["sign"], 0, 3),
                "orbit" => (string)$item["orbit"],
                "retro" => $item["retro"],
            ];
        }
        return $result;
    }
}
