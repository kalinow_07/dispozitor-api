<?php


namespace App\Service\Interpretation\SearchInterpretation;


class SearchInterpretationInterpretationAdapterService
{
    /**
     * @param $data
     * @return mixed
     * Need return [["sign" => "tel","planet" => "sol","retro" => true, "orbit" => 2]]
     */
    public function get(Array $data){
        $result = [];
        foreach ($data as $item){
            $result[] = [
                "planet" => $item["planet"]["id"],
                "sign" => isset($item["sign"]["id"]) ? $item["sign"]["id"] : false,
                "orbit" => isset($item["orbit"]["id"]) ? $item["orbit"]["id"] : false,
                "retro" => $item["retro"],
            ];
        }
        return $result;
    }

}
