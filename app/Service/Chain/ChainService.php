<?php


namespace App\Service\Chain;


use Carbon\Carbon;

class ChainService
{

    public function get(Carbon $date, Carbon $time, $chain_type){
        $date_for_chain = $date->format("Y-m-d") . "%20" . $time->format("H:i");
        $url = "https://api.astroreader.ru/public/chain/get?type=$chain_type&date=$date_for_chain";
        return file_get_contents($url);
    }
}
