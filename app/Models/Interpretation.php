<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Interpretation extends Model
{
    use HasFactory;
    protected $fillable = [
        "text",
        "options",
        "user_id",
        "index",
        "rating",
        "name",
        "link",
        "clip",
    ];
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    public function links(){
        return $this->hasMany('App\Models\InterpretationLink');
    }
    public function clips(){
        return $this->hasMany('App\Models\InterpretationClip');
    }
}
