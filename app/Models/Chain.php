<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chain extends Model
{
    protected $fillable = ['date', 'hour', 'data', 'chain_type'];
    use HasFactory;
}
