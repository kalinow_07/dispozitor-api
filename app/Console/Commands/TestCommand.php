<?php

namespace App\Console\Commands;

use App\Models\Chain;
use App\Models\Interpretation;
use App\Service\Chain\ChainService;
use App\Service\Interpretation\IndexInterpretation\IndexInterpretationService;
use App\Service\Interpretation\SearchInterpretation\SearchInterpretationChainAdapterService;
use App\Service\Interpretation\SearchInterpretation\SearchInterpretationInterpretationAdapterService;
use App\Service\Interpretation\SearchInterpretation\SearchInterpretationService;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;
use Illuminate\Console\Command;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $indexInterpretationSerivce = new IndexInterpretationService();
        $indexInterpretationSerivce->getInverseIndexByChain(Carbon::now(), Carbon::now());



        return true;
        $searchInterpretationService = new SearchInterpretationService();
        dump($searchInterpretationService->search(Carbon::now(), Carbon::now()));

        return true;

        $chainService = new ChainService();
        $result = $chainService->get(Carbon::now(), Carbon::now());
        //dump(json_decode($result, true)["result"]["nodes"]);
        $searchInterpretationChainAdapter = new SearchInterpretationChainAdapterService();
        $data = $searchInterpretationChainAdapter->get(json_decode($result, true)["result"]["nodes"]);
        dump($data);
        return true;
        $interpretation = Interpretation::first();
        $searchInterpretationInterpretationAdapterService = new SearchInterpretationInterpretationAdapterService();
        $result = $searchInterpretationInterpretationAdapterService->get(json_decode($interpretation->options, true)["planets"]);
        dump($result);
        return true;


        $array1 = ["sign" => "tel","planet" => "sol"];
        $array2 = [["planet" => "sol", "sign" => "tel2"], ["planet" => "sol", "sign" => "tel"]];
        dump(array_search($array1, $array2));
        return true;

        $query = Interpretation::whereRaw("JSON_CONTAINS(options, JSON_OBJECT('planets', JSON_ARRAY(JSON_OBJECT('planet', JSON_OBJECT('id','sol')))))");
        $result = $query->get();
        dump(count($result));
        return count($result);







        $interval = new CarbonInterval();
        $period = CarbonPeriod::create('1936-01-05', '1 hours', '2050-01-01');
        foreach ($period as $date) {
            $date_for_url = $date->format("Y-m-d%20H:i");
            $url = "https://api.astroreader.ru/public/chain/get?chain_type=wla&date=$date_for_url";
            $date_for_db = $date->format("Y-m-d");
            $hour_for_db = $date->format("H");
            $chain = Chain::where("date", $date_for_db)->where("hour", $hour_for_db)->first();
            if ((bool)$chain) continue;

            $chain_type = "wla";
            $data = file_get_contents($url);
            Chain::create([
                "date" => $date_for_db,
                "hour" => $hour_for_db,
                "chain_type" => $chain_type,
                "data" => $data,
            ]);
            $this->info($date_for_url);
        }

        return 1;
    }
}
